<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contidos

- [Manual para participantes](#manual-para-participantes)
    - [Instalación do cliente de Zoom](#instalación-do-cliente-de-zoom)
        - [Windows](#windows)
    - [Actualización do cliente](#actualización-do-cliente)
        - [Windows](#windows-1)
        - [Android](#android)
    - [Antes de entrar nunha reunión](#antes-de-entrar-nunha-reunión)
    - [Como entrar na reunión](#como-entrar-na-reunión)
        - [Dende Windows](#dende-windows)
        - [Dende móbil con Android](#dende-móbil-con-android)
        - [Chamada telefónica](#chamada-telefónica)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Manual para participantes

Neste manual faise unha presentación das operacións que debe realizar un participante (ou convidado) nunha reunión de Zoom.

## Instalación do cliente de Zoom

### Windows

Para realizar a instalación do cliente de Zoom nun sistema operativo Windows, debemos abrir un navegador e ir á [seguinte páxina](https://santiago.zoom.us/download) para iniciar a descarga:

	https://santiago.zoom.us/download

Que ten un aspecto similar á seguinte captura:

![image](/imaxes/zoom-instalacion-windows.png)

> Esta páxina recoñece o noso sistema operativo e xa nos ofrece a opción de **Descargar** o cliente específico para Windows. Se usamos outro sistema operativo diferente, aparecerá o cliente correspondente para dito sistema.

Facemos clic no botón **Descargar** que aparece no parágrafo *Cliente Zoom para reuniones*.

Unha vez rematada a descarga, comezará o proceso de instalación. Dependendo do navegador podemos ter que facer dobre clic no ficheiro descargado para comezar dita instalación.

Cando remate a instalación do cliente aparecerá unha xanela como a seguinte:

![image](/imaxes/zoom-participante-windows-01.png)

E, a partir deste momento, xa podemos facer uso do cliente para entrar nunha reunión, ou iniciar sesión coas nosas credenciais. Os empregados municipais poden entrar cos datos que deron de alta cando recibiron a invitación. Se non tes estes datos, ou tes problemas para iniciar sesión, debes contactar co CAU.

## Actualización do cliente

**É moi importante que usemos a última versión dispoñible do cliente de Zoom**. O equipo técnico de Zoom está implementando novas funcionalidades que mellorarán a calidade das nosas reunións, e sobre todo vainas facer **máis seguras**.

### Windows

Cando se abre o cliente de Zoom, podemos comprobar a versión que temos instalada na parte inferior da xanela principal:

![image](/imaxes/zoom-version-instalada.png)

> Se temos unha sesión iniciada, podemos comprobar a versión instalada a través do menú *Acerca de...*.

Para iniciar a actualización da versión instalada, tan só temos que iniciar o proceso de descarga e instalación igual que se fixo durante a instalación inicial dende a [páxina de descarga](https://santiago.zoom.us/download).

### Android

Nun dispositivo móbil é posible que a actualización se faga de xeito automático. De tódolos xeitos, podemos comprobar a versión instalada dende o menú de configuración:

![imaxge](/imaxes/zoom-version-android.png)

## Antes de entrar nunha reunión

Para poder participar nunha reunión de Zoom á que asistimos como convidados tan só precisamos dúas cousas:

1. Ter instalado un **cliente de Zoom** no dispositivo a través do cal me vou conectar (PC, portátil, móbil, etc.)
2. Coñecer o **identificador** (ID) da reunión.
3. [Opcional] Contrasinal de acceso.

Os últimos datos (o ID e o contrasinal) teñen que ser proporcionados polo anfitrión da reunión. E poderá envialo por calquera medio acordado entre os participantes.

O identificador dunha reunión ten o seguinte aspecto:

	340-485-394

Se nos chega unha mensaxe de correo electrónico coa invitación para participar nunha reunión, aparecerá unha ligazón na mensaxe similar á seguinte:

	https://santiago.zoom.us/j/340485394

e, con facer clic nesa ligazón (e tendo o cliente de Zoom instalado), xa poderemos acceder á reunión.


## Como entrar na reunión

### Dende Windows

Chegados ata este punto, só nos queda abrir o cliente de Zoom e seguir as seguintes indicacións.

A xanela principal deste cliente é semellante ás doutros sistemas operativos, e ten o seguinte aspecto:

![image](/imaxes/zoom-participante-windows-01.png)

Temos que facer clic no botón **Entrar a una reunión**, e aparece a seguinte xanela:

![image](/imaxes/zoom-participante-windows-02.png)

Nesta xanela debemos escribir o Identificador da reunión, e revisar o nome que desexamos que apareza unha vez entremos na reunión.

> Se xa fixemos uso deste cliente para outras reunións, no campo *ID* teremos un historial das mesmas, evitando ter que inserir de novo o Identificador.

Das opcións que hai na parte inferior, tan só comentar que hai certas ocasións nas que pode ser aconsellable entrar nunha reunión *sen audio* ou incluso *sen vídeo*, evitando molestias innecesarias ao resto de participantes.

### Dende móbil con Android

As seguintes capturas foron feitas dende un teléfono móbil con Android, e poderían existir pequenas diferencias entre versións diferentes.

O primeiro que imos ver ao abrir  Zoom é a seguinte pantalla:

![image](/imaxes/zoom-participante-android-01.png)

> Se contamos cun usuario asociado ao correo do Concello, podemos iniciar sesión facendo clic en *Ingresar*, pero iso verase noutra sección.

O que nos interesa neste momento é entrar nunha reunión da cal coñecemos o identificador (ID), polo que facemos clic en **Entrar a una reunión**, e chegamos á seguinte pantalla:

![image](/imaxes/zoom-participante-android-02.png)

Temos que cubrir dous campos de texto, o primeiro co **ID da reunión**, e o segundo co **noso nome**.

![image](/imaxes/zoom-participante-android-05.png)

No noso exemplo, imos entrar nunha reunión que ten o ID *627 078 934*.

Pode suceder que teñamos que volver a unha reunión na que xa estivemos participando hai un tempo, polo que aparece no historial de identificadores:

![image](/imaxes/zoom-participante-android-03.png)

Unha vez que facemos clic no botón **Entrar a la reunión**, e mentres non chegue o anfitrión quedamos á súa espera na seguinte pantalla:

![image](/imaxes/zoom-participante-android-06.png)

### Chamada telefónica

Este método permite que poidamos participnar nunha reunión cunha simple chamada telefónica. Pode ser a única opción posible se nos quedamos sen acceso a Internet.

Teremos que facer unha chamada a un dos seguintes números fixos:

* 91 787 3431
* 91 787 0058

Despois de escoitar a locución, escribimos co teclado do teléfono o ID da reunión seguido de dous caracteres `##`, por exemplo:

	627078934##

E xa estariamos en condicións de participar na reunión como se se tratase dunha *multiconferencia*.
