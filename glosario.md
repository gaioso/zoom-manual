# Glosario de termos

* Hospedador/a (Anfitrión, Organizador/a)
  * Persoa que se encarga de crear unha reunión programada, e de enviar a invitación aos participantes. Dependendo da configuración da reunión, o anfitrión debe entrar na reunión para que esta poida comezar. Terá o control sobre o resto de paricipantes durante a reunión, por exemplo para apagar os micrófonos. Calquera usuario pode organizar unha reunión, tendo en conta que se hai máis de 2 participantes, hai un límite de 40 minutos. Se é necesario manter unha reunión de maior duración debe contactar co CAU.
* Participante (Convidado/a)
  * Persoa que participa nunha reunión ao recibir unha mensaxe de invitación. Nesta mensaxe debe indicarse o ID da reunión para poder participar na mesma. Non precisa máis que ese ID e un cliente de Zoom. Se a reunión está organizada por persoal do Concello, a invitación pode enviarse a calquera persoa a través dunha mensaxe de correo electrónico, por exemplo, para que poida participar na mesma.
